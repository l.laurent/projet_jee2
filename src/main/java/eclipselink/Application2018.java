package eclipselink;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("appplication-path2018")
public class Application2018 extends ResourceConfig{
    public Application2018() {
        packages("package2018");
    }
}