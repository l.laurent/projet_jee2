package eclipselink;

import java.lang.reflect.Proxy;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.eclipse.persistence.internal.jpa.EntityManagerFactoryDelegate;

public class PersistenceManager {
	
	private static final PersistenceManager singleton = new PersistenceManager();
	
	protected ScopedEntityManagerFactory semf;
	
	public static PersistenceManager getInstance() {
		
		return singleton;
	}
	
	private PersistenceManager() {
		
		
	}
	
	public ScopedEntityManagerFactory getScopedEntityManagerFactory() {
		
		if (semf == null ) {
			
			createdScopedEntityManagerFactory();
		}
		
		return semf;
	}
	
	public void closeEntityManagerFactory() {
		
		if (semf != null) {
			semf.close();
			semf = null;
		}
	}
	
	private void createdScopedEntityManagerFactory() {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("MyPU");
		semf = (ScopedEntityManagerFactory) Proxy.newProxyInstance(
				ScopedEntityManagerFactory.class.getClassLoader(),
				new Class[] {ScopedEntityManagerFactory.class},
				new EntityManagerFactoryHandler(emf));
	}
}