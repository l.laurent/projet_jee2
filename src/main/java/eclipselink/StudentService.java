package eclipselink;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Stateless
@LocalBean
@Path("/students")
public class StudentService {

    @GET
    @Path("{id}")
    public String getId(@PathParam("id") long id) {
        return "Hello World" + id;
    }
    
  
}