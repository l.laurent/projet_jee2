package eclipselink;

import lombok.Data;

@Data
public class Student {
    String studentName;

    public Student(String studentName) {
        this.studentName = studentName;
    }
}