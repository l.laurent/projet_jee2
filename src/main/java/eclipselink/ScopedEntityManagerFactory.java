package eclipselink;

import javax.persistence.EntityManagerFactory;

public interface ScopedEntityManagerFactory extends EntityManagerFactory {
public ScopedEntityManager createScopedEntityManager();
}
