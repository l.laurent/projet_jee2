package eclipselink;

import javax.persistence.EntityManager;

public interface ScopedEntityManager extends EntityManager, AutoCloseable {
	
	public interface Transaction {
		
		public void execute();
	}
	
	public interface TransactionFunction<T> {
		
		public T execute();
	}
	
	
	public void executeTransaction(Transaction t);
	public <T> T executeTransaction(TransactionFunction<T> t);

}
